﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController Instance {get; private set;}
	public bool allowExplodes = false;

	#region Private Fields
	
	List<GameObject> Levels = new List<GameObject>(); // The list of levels as prefabs
	Vector3 levelPosition = new Vector3(27, 0, 0); // where levels get placed on Instantiation
	int shots; // the number of shots alotted for the current loaded level
	int reqs; // the number of required destructibles remaining in the loaded current level
	Stack<GameObject> crushers = new Stack<GameObject>(); // a stack for using crushers
	List<GameObject> cleanupCrushers = new List<GameObject>(); // a list for cleaning up crushers (and their particle trails) after a level

	Vector3 crusherIdlePosition = new Vector3(-7.5f, -2.5f, 0);

	const float COLLISION_TIME = 2; // the number of seconds after a collision before the camera moves back to the slingshot (unless another collison occurs)
	float collisionTimer; // used for counting up to COLLISION_TIME
	
	// various level properties
	string levelName;
	GameObject currentLevel;
	int currentLevelNumber;
	bool levelComplete;
	int levelScore;
	int levelBest;

	// references that are setup in the awake method
	CameraMovement cameraMovement;
	TextMesh scoreTextMesh;
	TextMesh bestTextMesh;
	TextMesh levelNameTextMesh;
	SlingSeat slingSeat;

	#endregion

	void Awake () {
		Instance = this;
		Application.targetFrameRate = 60;
		// load refernces to the level prefabs
		foreach (object o in Resources.LoadAll("Levels")) {
			Levels.Add((GameObject)o);
		}
		slingSeat = GameObject.Find("SlingShot/slingSeat").GetComponent<SlingSeat>();
		scoreTextMesh = GameObject.FindGameObjectWithTag("GUICamera").transform.FindChild("TopLeft/ScoreNumber").GetComponent<TextMesh>();
		bestTextMesh = GameObject.FindGameObjectWithTag("GUICamera").transform.FindChild("TopLeft/BestNumber").GetComponent<TextMesh>();
		levelNameTextMesh = GameObject.FindGameObjectWithTag("GUICamera").transform.FindChild("TopRight/LevelNameText").GetComponent<TextMesh>();
		cameraMovement = Camera.main.GetComponent<CameraMovement>();
	}

	void Start () {
		// start the game by showing the level select screen
		LevelSelect.Instance.Show();
	}

	public void Retry () {
		SpawnLevel (currentLevelNumber);
	}

	/*
	 * Reset
	 *
	 * The reset method allows an in progress level to properly (and nicely) terminate, clean up, and then
	 * either return to the level select screen, or simple respawn the level. This function is called by
	 * the transulcent gray buttons visible only while a level is loaded up and in progress. 
	 *
	 */

	public void Reset (int res) {
		StopAllCoroutines();

		int d = 0;
		for (int i=0; i < cleanupCrushers.Count; i++) {
			d ++;
			cleanupCrushers[i].GetComponent<CandyCrusher>().CleanupTrail();
			if (cleanupCrushers[i].activeSelf) {
				cleanupCrushers[i].GetComponent<CandyCrusher>().Explode();

			}
		}
		allowExplodes = false;
		cleanupCrushers.Clear();
		crushers.Clear();
		slingSeat.Reset();
		cameraMovement.Reset();
		GameObject.FindGameObjectWithTag("GUICamera").animation.Play("hide");
		if (res == 0)
			StartCoroutine(ResetToLevelSeclet());
		if (res == 1)
			StartCoroutine(ResetToRetry());
	}

	IEnumerator ResetToLevelSeclet () {
		yield return new WaitForSeconds(.5f);
		LevelSelect.Instance.Show();
	}

	IEnumerator ResetToRetry () {
		yield return new WaitForSeconds(.5f);
		Retry ();
	}

	public void SpawnLevel (int levelNumber) {
		cameraMovement.PanTo(CameraMovement.levelPos);
		levelName = "Level" + levelNumber.ToString();

		if (currentLevel)
			Destroy(currentLevel);
		for (int i = 0; i < Levels.Count; i ++) {
			if (Levels[i].name == levelName) {
				currentLevel = GameObject.Instantiate(Levels[i]) as GameObject;
				break;
			}
		}

		// Error checking for if a Level is mislabeled/non existant but attmepted to be created
		if (!currentLevel) {
			Debug.LogError("Error: no Level named " + levelName + " exists in Resources/Levels.");
			return;
		}

		//Grab all the stats from the level, position the level, show the GUI, and then start the game
		currentLevel.name = levelName;
		currentLevelNumber = levelNumber;
		currentLevel.transform.position = levelPosition;
		levelScore = 0;
		shots = currentLevel.GetComponent<LevelInfo>().Shots;

		// itterate over the destructible objects in the loaded level, and check how many are required
		// for level completion, stored in the variable reqs
		Destructible[] destructibles = currentLevel.GetComponentsInChildren<Destructible>();
		reqs = 0;
		for (int i=0; i < destructibles.Length; i++) {
			if (destructibles[i].required)
				reqs ++;
		}
		levelComplete = false;
		levelScore = 0;
		levelBest = PlayerPrefs.GetInt(levelName + "Score");
		scoreTextMesh.text = levelScore.ToString();
		bestTextMesh.text = levelBest.ToString();
		levelNameTextMesh.text = "Level " + levelNumber.ToString();

		GameObject.FindGameObjectWithTag("GUICamera").animation.Play("show");
		InGameButtons.Singleton.Show();

		StartCoroutine(StartLevel ());
	}

	// After 3 seconds, Tell the camera to pan back to the start and let the player start firing, spawn the crushers
	IEnumerator StartLevel () {
		yield return new WaitForSeconds(3);
		while (crushers.Count != 0) {
			//clear the crushers Stack if any were sitting in it Idle
			ObjectPoolManager.Instance.Push(crushers.Pop());
		}
		for (int i = 0; i < shots; i++) {
			GameObject newCrusher = ObjectPoolManager.Instance.Pop("candyCrusher");
			newCrusher.GetComponent<CandyCrusher>().Reset();
			crushers.Push(newCrusher);
			cleanupCrushers.Add(newCrusher);
			newCrusher.SetActive(true);
			newCrusher.transform.position = crusherIdlePosition + new Vector3(i * 1f, 0, -i); // place the new crushers 1.5 units apart
		}
		cameraMovement.PanTo(CameraMovement.slingPos);
		yield return new WaitForSeconds(2);
		crushers.Pop().GetComponent<CandyCrusher>().LoadIntoSling();
	}

	public void AwardPoints (int points, Vector3 pointSpawn) {
		levelScore += points;
		GameObject scorePopup = ObjectPoolManager.Instance.Pop("ScorePopup");
		scorePopup.SetActive(true);
		scorePopup.GetComponent<ScorePopup>().Show(points, pointSpawn);
		scoreTextMesh.text = levelScore.ToString();
		if (levelScore > levelBest)
			bestTextMesh.text = levelScore.ToString();
	}

	// when a required destructible is destroyed, it tells the game to decrement the count of required objects remaining,
	// and checks if there are anymore requireds remaining (at which point the player wins the level)
	public void RequiredDestroyed () {
		reqs--;
		if (reqs <= 0)
			LevelComplete();
	}

	void LevelComplete () {
		levelComplete = true;
		AudioController.Instance.PlaySound("Cheering");
	}

	// collisions in game world call this
	public void ResetCollisionTimer () {
		collisionTimer = COLLISION_TIME;
	}

	// called by a candy crusher when it makes first contact after being launched
	public void StartCollisionTimeout () {
		StartCoroutine(CollisionTimeout());
	}

	//if no collisions are recieved for 3 after first contact, evaluate win condition and either set up another shot or end the game!
	IEnumerator CollisionTimeout () {
		ResetCollisionTimer ();
		while (collisionTimer > 0) {
			collisionTimer -= Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		allowExplodes = false;
		cameraMovement.PanTo(CameraMovement.slingPos);
		yield return new WaitForSeconds(2);

		// win condition, award bonus points for remaining crushers and end the game
		if (levelComplete)
			StartCoroutine(WinLogic());
		// game still in play condtion, load another crusher into the sling
		else if (crushers.Count > 0) 
			crushers.Pop().GetComponent<CandyCrusher>().LoadIntoSling();
		// loss condition, game over
		else 
			StartCoroutine(LoseLogic());
	}

	IEnumerator WinLogic () {
		while (crushers.Count > 0) {
			crushers.Pop().GetComponent<CandyCrusher>().BonusPoints();
			if (crushers.Count > 0)
				yield return new WaitForSeconds(0.3f);
		}
		
		for (int i=0; i< cleanupCrushers.Count; i++) {
			cleanupCrushers[i].GetComponent<CandyCrusher>().CleanupTrail();
			if (cleanupCrushers[i].activeSelf)
				ObjectPoolManager.Instance.Push(cleanupCrushers[i]);
		}
		cleanupCrushers.Clear();
		GameObject.FindGameObjectWithTag("GUICamera").animation.Play("hide");
		InGameButtons.Singleton.Hide();
		
		yield return new WaitForSeconds(0.5f);

		int[] starScores = currentLevel.GetComponent<LevelInfo>().StarScores;
		int starsEarned = 0;

		if (levelScore > starScores[2])
			starsEarned = 3;
		else if (levelScore > starScores[1])
			starsEarned = 2;
		else if (levelScore > starScores[0])
			starsEarned = 1;

		// Save Stars earned and new best to Playerprefs if this score beat the previous best
		if (levelScore >= levelBest) {
			PlayerPrefs.SetInt(levelName + "Stars", starsEarned);
			PlayerPrefs.SetInt(levelName + "Score", levelScore);
		}

		LevelTotalsScreen.Singleton.Show (levelScore, starsEarned, currentLevel.GetComponent<LevelInfo>().StarScores);
	}

	IEnumerator LoseLogic () {
		for (int i=0; i< cleanupCrushers.Count; i++) {
			cleanupCrushers[i].GetComponent<CandyCrusher>().CleanupTrail();
			if (cleanupCrushers[i].activeSelf)
				cleanupCrushers[i].GetComponent<CandyCrusher>().Explode();
		}
		cleanupCrushers.Clear();
		GameObject.FindGameObjectWithTag("GUICamera").animation.Play("hide");
		InGameButtons.Singleton.Hide();
		LevelLossScreen.Singleton.Show();
		yield return null;
	}
}
