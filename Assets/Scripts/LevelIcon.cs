﻿using UnityEngine;
using System.Collections;

public class LevelIcon : MonoBehaviour {

	public int LevelNumber;
	public int requiredStars;
	public bool levelLocked;
	string levelName;
	GameObject levelLock;
	GameObject star1;
	GameObject star2;
	GameObject star3;

	void Awake () {
		levelName = "Level" + LevelNumber.ToString();
		GetComponentInChildren<TextMesh>().text = LevelNumber.ToString();
		levelLock = transform.FindChild("levelLock").gameObject;
		star1 = transform.FindChild("Stars/Star1").gameObject;
		star2 = transform.FindChild("Stars/Star2").gameObject;
		star3 = transform.FindChild("Stars/Star3").gameObject;
		star1.SetActive(false);
		star2.SetActive(false);
		star3.SetActive(false);
	}

	public void RefreshIcon () {
		// Decide whether this levelIcon should be locked based on whether the previous level has been completed
		// also show the appropriate number of stars unlocked for this level

		if (LevelNumber != 1)
			if (PlayerPrefs.GetInt("Level" + (LevelNumber-1).ToString() + "Score") == 0)
				levelLocked = true;
		else
			levelLocked = false;
		levelLock.SetActive(levelLocked);

		int numberOfStars = PlayerPrefs.GetInt(levelName + "Stars"); // the number of stars for this specific level
		if (numberOfStars > 0)
			star1.SetActive(true);
		if (numberOfStars > 1)
			star2.SetActive(true);
		if (numberOfStars > 2)
			star3.SetActive(true);
	}
}
