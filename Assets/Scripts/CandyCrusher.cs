using UnityEngine;
using System.Collections;

public class CandyCrusher : MonoBehaviour {

	public AnimationCurve jumpCurve; // used to make loading into the sling look more natural
	public Sprite neutralFace;
	public Sprite snarkyFace;
	public bool launched;

	float health;
	bool firstContact = false; // 
	ParticleSystem trail;
	Transform slingSeat;
	const int BONUS = 10000;
	const int BASE_HEALTH = 2000;
	const int MAX_DAMAGE = 1000; // maximum damage you can take in one frame
	float damageThisFrame;

	ParticleSystem.Particle[] particles = new ParticleSystem.Particle[100];

	void Awake () {
		slingSeat = GameObject.Find("SlingShot/slingSeat").transform;
	}

	// Reset so when this crusher is used by the asset pool it gets things set back to defaults
	public void Reset () {
		health = BASE_HEALTH;
		launched = false;
		transform.FindChild("candyCrusherBody/face").GetComponent<SpriteRenderer>().sprite = neutralFace;
		transform.eulerAngles = Vector3.zero;
		animation.Play ("idle");
		animation["idle"].time = Random.Range(0, animation["idle"].length);
		firstContact = false;
		collider2D.isTrigger = true;
		rigidbody2D.isKinematic = true;
	}

	/*
	 * CleanupTrail
	 * 
	 * It would have been easy to simple do particleSystem.clear() to clean up left over particle trails...
	 * but that doesn't look nice. Instead, the first particle in the array (the oldest particle) is looked at, and
	 * its age is used to deterimine an amount to reduce the other particle's lifeTimes by. This results in particle trails
	 * doing a cleaner looking animation of each particle scaling down when a level is reset/completed.
	 * 
	 */

	public void CleanupTrail () {
		if (trail) {
			trail.Stop();
			int count = trail.GetParticles(particles);
			float lifeTimeReduce = particles[0].lifetime - 1;
			for (int i = 0; i<count; i++) {
				particles[i].lifetime -= lifeTimeReduce;
			}
			trail.SetParticles(particles, count);
			trail.transform.parent = null;
			trail = null;
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (!firstContact) {
			trail.Stop();
			trail.transform.parent = null;
			GameController.Instance.StartCollisionTimeout();
			firstContact = true;
		}
		// return if the collider is null (ie, it just get destroyed) OR if it's the ground (ie, the ground doesn't deal damage to crushers)
		if (!collision.collider || collision.collider.CompareTag("Ground"))
			return;
		// damage is based on the velocity the crusher is travelling when it collides
		TakeDamage(collision.relativeVelocity.sqrMagnitude);
	}
	
	void TakeDamage (float damage) {
		// cap off how much damage you can take each frame
		damageThisFrame += damage;
		if (damageThisFrame < MAX_DAMAGE)
			health -= damage;
		else
			health -= damage - (damageThisFrame - MAX_DAMAGE);
		if (health <= 0)
			Explode();
	}

	// used when looping over extra crushers and rewarding bonus points on level completion
	public void BonusPoints () {
		GameController.Instance.AwardPoints(BONUS, transform.position);
		AudioController.Instance.PlaySound("BonusPoints");
		Explode();
	}

	public void Explode () {
		ParticleSystem pSystem = ObjectPoolManager.Instance.Pop("crusherPuff").GetComponent<ParticleSystem>();
		pSystem.gameObject.SetActive(true);
		pSystem.transform.position = transform.position;
		pSystem.transform.eulerAngles = transform.eulerAngles;
		pSystem.Play();
		ObjectPoolManager.Instance.Push(gameObject);
	}

	// plays the jump animation, which calls back to actually load the sling
	public void LoadIntoSling () {
		animation.Play("jump");
		AudioController.Instance.PlaySound("Jump");
	}

	// this is called as an Animation event in the "jump" animation
	void JumpCallBack () {
		StartCoroutine(ILoadIntoSling());
	}

	// load the crusher into the sling, done inside of a coroutine for timing reasons
	IEnumerator ILoadIntoSling () {
		float t = 0;
		Vector3 startPos = transform.position;
		while (t < 1f) {
			t = Mathf.Min (t + Time.deltaTime * 3, 1f);
			// this next magical line makes the crusher nicely jump into the sling seat no matter where he starts from and where the sling is
			transform.position = new Vector3(Mathf.Lerp(startPos.x, slingSeat.position.x, t), startPos.y * (1 - jumpCurve.Evaluate(t)) + slingSeat.position.y * jumpCurve.Evaluate(t));
			transform.eulerAngles = Vector3.forward * Mathf.LerpAngle(0, slingSeat.transform.eulerAngles.z, t);
			yield return new WaitForEndOfFrame();
		}
		slingSeat.GetComponent<SlingSeat>().Load(this);
	}

	public void Launch (Vector2 launchForce) {
		GameController.Instance.allowExplodes = true;
		trail = ObjectPoolManager.Instance.Pop("trail").particleSystem;
		trail.gameObject.SetActive(true);
		trail.transform.position = transform.position;
		trail.transform.parent = transform;
		trail.Play();
		transform.FindChild("candyCrusherBody/face").GetComponent<SpriteRenderer>().sprite = snarkyFace;
		launched = true;
		collider2D.isTrigger = false;
		rigidbody2D.isKinematic = false;
		rigidbody2D.velocity = launchForce;
	}
	
	void Update () {
		damageThisFrame = 0;
		// if the crusher has been launched, but hasn't made contact yet, lerp its angle such that its spiky bottom side is pointing down
		if (launched && !firstContact) {
			transform.eulerAngles = Vector3.forward * Mathf.LerpAngle(transform.eulerAngles.z, 0, Time.deltaTime);
		}
	}
}
