using UnityEngine;
using System.Collections;

public class LevelLossScreen : MonoBehaviour {
	
	public static LevelLossScreen Singleton;
	
	Transform fadePlane;
	TextMesh scoreText;
	Transform buttons;
	bool acceptInputs = false;
	Vector3 zoomedScale = new Vector3 (1.5f, 1.5f, 1.5f);
	
	void Awake () {
		Singleton = this;
		fadePlane = transform.FindChild("FadePlane");
		scoreText = transform.FindChild("FinalScoreText").GetComponent<TextMesh>();
		buttons = transform.FindChild("Buttons");
		gameObject.SetActive(false);
	}
	
	public void Show () {
		acceptInputs = true;
		gameObject.SetActive(true);
		transform.localScale = zoomedScale;
		fadePlane.gameObject.SetActive(true);
		StartCoroutine(IShow ());
	}
	
	public void Hide () {
		acceptInputs = false;
		StartCoroutine(IHide());
	}
	
	IEnumerator IHide () {
		yield return new WaitForSeconds (0.2f);
		buttons.animation.Play("hideButtons");
		yield return new WaitForSeconds (0.1f);
		float t = 0;
		while (t < 1) {
			t = Mathf.Min (1, t + Time.deltaTime * 5);
			transform.localScale = Vector3.Lerp (Vector3.one, zoomedScale, t);
			fadePlane.renderer.material.SetColor("_TintColor", Color32.Lerp(new Color32(0,0,0,100), Color.clear, t));
			scoreText.color = Color32.Lerp(new Color32(255,255,255,255), Color.clear, t);
			yield return new WaitForEndOfFrame();
		}
		gameObject.SetActive(false);
	}
	
	IEnumerator IShow () {
		float t = 0;
		buttons.animation.Play("showButtons");
		while (t < 1) {
			t = Mathf.Min (1, t + Time.deltaTime * 5);
			transform.localScale = Vector3.Lerp (zoomedScale, Vector3.one, t);
			fadePlane.renderer.material.SetColor("_TintColor", Color32.Lerp(Color.clear, new Color32(0,0,0,100), t));
			scoreText.color = Color32.Lerp(Color.clear, new Color32(255,255,255,255), t);
			yield return new WaitForEndOfFrame();
		}
		yield return null;
	}

	void ShowLevelSelect () {
		StartCoroutine(IShowLevelSelect());
	}
	
	IEnumerator IShowLevelSelect () {
		yield return new WaitForSeconds(0.2f);
		LevelSelect.Instance.Show();
	}

	void Update () {
		if (InputHandler.GetInstance().inputSignalDown && acceptInputs) {
			Collider2D col = Physics2D.OverlapPoint(GameObject.FindGameObjectWithTag("GUICamera").camera.ScreenToWorldPoint(InputHandler.GetInstance().inputVector), 512);
			
			if (col) {
				if (col.name == "homeButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					ShowLevelSelect ();
					Hide ();
				}
				
				else if (col.name == "retryButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					GameController.Instance.Retry();
					Hide ();
				}
			}
		}
	}
	
}
