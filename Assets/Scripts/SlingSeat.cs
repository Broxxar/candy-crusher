﻿using UnityEngine;
using System.Collections;

public class SlingSeat : InteractableObject {

	bool dragging;
	CandyCrusher loadedCrusher;

	public AnimationCurve elasticEase;
	Vector3 origin;
	Vector3 loadedPosition;

	float magnitudeLastFrame = 0;
	float volume;

	Vector3 releaseVector;
	float maxDistance = 3; //maximum distance the seat can be pulled away from the sling
	float minMagnitude = 1.5f; //minimum amount of distance between seat and sling to launch

	void Awake () {
		origin = transform.position;
		loadedPosition = origin - new Vector3(0.3f, 0.2f);
	}

	void StartDrag () {
		dragging = true;
	}

	public void Load(CandyCrusher cc) {
		loadedCrusher = cc;
		cc.transform.parent = transform;
		cc.transform.localPosition += new Vector3(0.5f, 0);
		cc.transform.eulerAngles = transform.eulerAngles;

		releaseVector = new Vector3(-0.5f, -0.5f);
		StartCoroutine(LoadedReset());
	}

	public void Reset() {
		if (loadedCrusher) {
			loadedCrusher = null;
			releaseVector = new Vector3(-0.5f, -0.5f);
			StartCoroutine(OriginReset());
		}
		if (dragging) {
			Release ();
		}
	}

	void Release () {
		releaseVector = transform.position - origin;
		dragging = false;

		if (transform.position.x < origin.x - 0.5f && releaseVector.magnitude > minMagnitude) {
			if (loadedCrusher) {
				Camera.main.GetComponent<CameraMovement>().SetTarget(loadedCrusher.transform);
				loadedCrusher.Launch(((Vector2)(origin - transform.position)) * 12);
				loadedCrusher.transform.parent = null;
				loadedCrusher = null;
			}
		}
		if (loadedCrusher)
			StartCoroutine(LoadedReset());
		else
			StartCoroutine(OriginReset());
	}

	IEnumerator LoadedReset () {
		float elasticTime = 1.5f;
		float t = 0;
		
		while (t < elasticTime && !dragging) {
			yield return new WaitForEndOfFrame();
			t = Mathf.Min (elasticTime, t + Time.deltaTime);
			transform.eulerAngles = Vector3.forward * Mathf.LerpAngle(transform.eulerAngles.z, 15, t);
			transform.position = loadedPosition + (releaseVector * Mathf.Sin (t * 10f + Mathf.PI/2) * elasticEase.Evaluate((elasticTime - t)/elasticTime));
		}
	}

	IEnumerator OriginReset () {
		float elasticTime = 1f;
		float t = 0;

		while (t < elasticTime && !dragging) {
			t = Mathf.Min (elasticTime, t + Time.deltaTime);
			transform.eulerAngles = Vector3.forward * Mathf.LerpAngle(transform.eulerAngles.z, 0, t);
			transform.position = origin + (releaseVector * Mathf.Sin (t * 20f + Mathf.PI/2) * elasticEase.Evaluate((elasticTime - t)/elasticTime));
			yield return new WaitForEndOfFrame();
		}
	}

	//Changes the pitch and volume of a looping sine wave to sound like a stretching elastic!
	void UpdateSound () {
		volume = Mathf.Min (0.5f, volume + Mathf.Abs(magnitudeLastFrame - (transform.position - origin).magnitude));
		volume = Mathf.Lerp(volume, 0, Time.deltaTime * 10);
		audio.pitch = Mathf.Max (0, (transform.position - origin).magnitude)/1.6f;
		audio.volume = volume;
		magnitudeLastFrame = (transform.position - origin).magnitude;
	}

	void Update () {

		UpdateSound();

		if (InputHandler.GetInstance().inputSignalDown) {
			Collider2D col = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(InputHandler.GetInstance().inputVector), 256);
			if (col) {
				if (col.gameObject == this.gameObject)
					StartDrag();
			}
		}

		if (dragging) {
			if (InputHandler.GetInstance().inputSignalUp)
				Release();

			transform.eulerAngles = Vector3.forward * (Mathf.Atan2 (origin.y - transform.position.y, origin.x - transform.position.x) * Mathf.Rad2Deg);

			transform.position = (Vector2)Camera.main.ScreenToWorldPoint(InputHandler.GetInstance().inputVector);
			if (Vector3.Distance(transform.position, origin) > maxDistance) {
				transform.position = origin + (transform.position - origin).normalized * maxDistance;
			}
		}
	}
}
