﻿using UnityEngine;
using System.Collections;

public class SlingShot : MonoBehaviour {

	// The actual physics of grabbing the slingshot, pulling back and firing occurs
	// in the sling seat object, this sling shot class simply takes care of the streatching of
	// the rubber bands based on the position of the seat.

	Transform slingSeat;
	Transform frontSlingBand;
	Transform backSlingBand;

	float frontSlingInitialDistance;
	float backSlingInitialDistance;
	float frontSlingInitalXScale;
	float backSlingInitalXScale;
	
	void Awake () {
		slingSeat = transform.FindChild("slingSeat");
		frontSlingBand = transform.FindChild("slingFront/slingBand");
		backSlingBand = transform.FindChild("slingBack/slingBand");
		frontSlingInitialDistance = Vector3.Distance(slingSeat.position, frontSlingBand.position);
		backSlingInitialDistance = Vector3.Distance(slingSeat.position, backSlingBand.position);
		frontSlingInitalXScale = frontSlingBand.localScale.x;
		backSlingInitalXScale = backSlingBand.localScale.x;
	}

	void Update () {
		backSlingBand.eulerAngles = Vector3.forward * (Mathf.Atan2(slingSeat.position.y - backSlingBand.position.y, slingSeat.position.x - backSlingBand.position.x) * Mathf.Rad2Deg - 180);
		frontSlingBand.eulerAngles = Vector3.forward * (Mathf.Atan2(slingSeat.position.y - frontSlingBand.position.y, slingSeat.position.x - frontSlingBand.position.x) * Mathf.Rad2Deg - 180);
		frontSlingBand.localScale = new Vector3(Vector3.Distance(slingSeat.position, frontSlingBand.position)/frontSlingInitialDistance * frontSlingInitalXScale, 1, 1);
		backSlingBand.localScale = new Vector3(Vector3.Distance(slingSeat.position, backSlingBand.position)/backSlingInitialDistance * backSlingInitalXScale, 1, 1);
	}
}
