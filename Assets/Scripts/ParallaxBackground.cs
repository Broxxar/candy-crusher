﻿using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour {

	Material nearBg;
	Material farBg;
	
	void Awake () {
		nearBg = transform.FindChild("NearBG").renderer.sharedMaterial;
		farBg = transform.FindChild("FarBG").renderer.sharedMaterial;
	}

	void Update () {
		nearBg.mainTextureOffset = new Vector2(transform.parent.position.x / 60, 0);
		farBg.mainTextureOffset = new Vector2(transform.parent.position.x / 120, 0);
	}
}
