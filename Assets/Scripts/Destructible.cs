using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Destructible : MonoBehaviour {

	public float health;// sqrMagnitude damage the object can take from collisions before it's explode method is called
	public List<string> deathParticles;// particle system names to grab from the pooled objects and play
	public List<string> deathSounds;// sound effects names to play from the audiocontroller
	public bool required;// is this destructible required to win
	public int points;//how many points this object is worth

	void OnCollisionEnter2D (Collision2D collision) {
		rigidbody2D.isKinematic = false;
		if (collision.relativeVelocity.sqrMagnitude > 5)
			GameController.Instance.ResetCollisionTimer();
 		TakeDamage(collision.relativeVelocity.sqrMagnitude, collision.collider);
	}

	void TakeDamage (float damage, Collider2D source) {
		health -= damage;
		if (health <= 0) {
			if (!GameController.Instance.allowExplodes) {
				health = 1;
				return;
			}
			if (source && source.CompareTag("Crusher") && !required)
				GameController.Instance.AwardPoints(points + Mathf.RoundToInt(damage/10) * 10, transform.position);
			else
				GameController.Instance.AwardPoints(points, transform.position);
			//if you're allowed to explode, do, otherwise, set health to 1
			//this prevents things from being broken off screen/after the game has ended

				Explode();
			return;
		}
		if (source && source.CompareTag("Crusher") && !required) {
			int damageBonus = Mathf.RoundToInt(damage/10) * 10;
			if (damageBonus > 0)
				GameController.Instance.AwardPoints(damageBonus, transform.position);
		}
	}

	void Explode () {
		if (required)
			GameController.Instance.RequiredDestroyed();

		if (deathSounds.Count > 0)
			AudioController.Instance.PlaySound(deathSounds[Random.Range(0, deathSounds.Count)]);

		for (int i=0; i<deathParticles.Count; i++) {
			ParticleSystem pSystem = ObjectPoolManager.Instance.Pop(deathParticles[i]).GetComponent<ParticleSystem>();
			pSystem.gameObject.SetActive(true);
			pSystem.transform.position = transform.position;
			pSystem.transform.eulerAngles = transform.eulerAngles;
			pSystem.Play();
		}
		Destroy(gameObject);
	}
}
