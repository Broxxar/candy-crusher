﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour {

	public virtual void DownAction () {}
	public virtual void UpAction () {}

}
