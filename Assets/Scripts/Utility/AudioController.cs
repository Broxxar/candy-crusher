using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour {
	
	Dictionary<string, AudioClip> SFX = new Dictionary<string, AudioClip>();
	Dictionary<string, AudioClip> Loops = new Dictionary<string, AudioClip>();
	public static AudioController Instance {get; private set;}
	AudioSource source;
	
	public string InitialSong;
		
	void Awake() {
		if (Instance) {
			Destroy(gameObject);
			return;
		}
		Instance = this;
		source = GetComponent<AudioSource>();
		DontDestroyOnLoad(gameObject);
		foreach (Object o in Resources.LoadAll("SFX")) {
			SFX.Add(o.name, (AudioClip)o);
		}
		foreach (Object o in Resources.LoadAll("Loops")) {
			Loops.Add(o.name, (AudioClip)o);
		}
		
		CrossFadeLoop(InitialSong);
	}

	// Attempt to play the sound from the SFX dictionary with the key: soundName
	public void PlaySound(string soundName) {
		AudioClip wav = null;
		SFX.TryGetValue(soundName, out wav);
		if (wav)
			AudioSource.PlayClipAtPoint(wav, Vector3.zero, 1);
	}
	
	// Attempt to fade in the loop from the Loops dictionary with the key: loopName
	public void CrossFadeLoop(string loopName) {
		AudioClip wav = null;
		Loops.TryGetValue(loopName, out wav);
		if (wav)
			StartCoroutine(Cross(wav));
	}

	public void MuteMusic () {
		audio.volume = 0;
	}

	IEnumerator Cross (AudioClip newLoop) {
		//If a loop is playing, fade it out linearly over time
		if (source.clip) {
			while (source.volume != 0) {
				source.volume = Mathf.Max (0, source.volume - Time.deltaTime);
				yield return new WaitForEndOfFrame();
			}
		}
		//Fade in the new loop linearly over time
		source.clip = newLoop;
		source.volume = 0;
		source.Play();
		while (source.volume != 0.5f) {
			source.volume = Mathf.Min (0.5f, source.volume + Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}
	}
}
