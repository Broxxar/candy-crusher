using UnityEngine;
using System.Collections;

public class LevelTotalsScreen : MonoBehaviour {

	public static LevelTotalsScreen Singleton;

	Transform fadePlane;
	TextMesh scoreText;
	TextMesh scoreNumber;
	Transform star1;
	Transform star2;
	Transform star3;
	int starsEarned;
	Transform buttons;
	bool acceptInputs = false;
	Vector3 zoomedScale = new Vector3 (1.5f, 1.5f, 1.5f);

	void Awake () {
		Singleton = this;
		fadePlane = transform.FindChild("FadePlane");
		scoreText = transform.FindChild("FinalScoreText").GetComponent<TextMesh>();
		scoreNumber = transform.FindChild("FinalScoreNumber").GetComponent<TextMesh>();
		star1 = transform.FindChild("Stars/Star1");
		star2 = transform.FindChild("Stars/Star2");
		star3 = transform.FindChild("Stars/Star3");
		buttons = transform.FindChild("Buttons");
		gameObject.SetActive(false);
	}

	public void Show (int score, int starsEarned, int[] reqScores) {
		acceptInputs = true;
		gameObject.SetActive(true);
		scoreNumber.text = score.ToString();
		transform.localScale = zoomedScale;
		StartCoroutine(IShow (starsEarned, reqScores));
	}

	public void Hide () {
		acceptInputs = false;
		StartCoroutine(IHide());
	}

	IEnumerator IHide () {
		yield return new WaitForSeconds (0.2f);
		buttons.animation.Play("hideButtons");

		if (starsEarned > 0)
			star1.animation.Play("starout");
		if (starsEarned > 1)
			star2.animation.Play("starout");
		if (starsEarned > 2)
			star3.animation.Play("starout");
		
		yield return new WaitForSeconds (0.1f);
		float t = 0;
		while (t < 1) {
			t = Mathf.Min (1, t + Time.deltaTime * 5);
			transform.localScale = Vector3.Lerp (Vector3.one, zoomedScale, t);
			fadePlane.renderer.material.SetColor("_TintColor", Color32.Lerp(new Color32(0,0,0,100), Color.clear, t));
			scoreText.color = Color32.Lerp(new Color32(255,255,255,255), Color.clear, t);
			scoreNumber.color = Color32.Lerp(new Color32(255,255,255,255), Color.clear, t);
			star1.GetComponent<SpriteRenderer>().color = Color32.Lerp(new Color32(50,50,50,50), Color.clear, t);
			star1.GetComponentInChildren<TextMesh>().color = Color32.Lerp(new Color32(255,255,255,100), Color.clear, t);
			star2.GetComponent<SpriteRenderer>().color = Color32.Lerp(new Color32(50,50,50,50), Color.clear, t);
			star2.GetComponentInChildren<TextMesh>().color = Color32.Lerp(new Color32(255,255,255,100), Color.clear, t);
			star3.GetComponent<SpriteRenderer>().color = Color32.Lerp(new Color32(50,50,50,50), Color.clear, t);
			star3.GetComponentInChildren<TextMesh>().color = Color32.Lerp(new Color32(255,255,255,100), Color.clear, t);
			yield return new WaitForEndOfFrame();
		}
		gameObject.SetActive(false);
	}

	IEnumerator IShow (int starsEarned, int[] reqScores) {
		float t = 0;

		star1.GetComponentInChildren<TextMesh>().text = reqScores[0].ToString();
		star2.GetComponentInChildren<TextMesh>().text = reqScores[1].ToString();
		star3.GetComponentInChildren<TextMesh>().text = reqScores[2].ToString();

		while (t < 1) {
			t = Mathf.Min (1, t + Time.deltaTime * 5);
			transform.localScale = Vector3.Lerp (zoomedScale, Vector3.one, t);
			fadePlane.renderer.material.SetColor("_TintColor", Color32.Lerp(Color.clear, new Color32(0,0,0,100), t));
			scoreText.color = Color32.Lerp(Color.clear, new Color32(255,255,255,255), t);
			scoreNumber.color = Color32.Lerp(Color.clear, new Color32(255,255,255,255), t);
			star1.GetComponent<SpriteRenderer>().color = Color32.Lerp(Color.clear, new Color32(50,50,50,50), t);
			star1.GetComponentInChildren<TextMesh>().color = Color32.Lerp(Color.clear, new Color32(255,255,255,100), t);
			star2.GetComponent<SpriteRenderer>().color = Color32.Lerp(Color.clear, new Color32(50,50,50,50), t);
			star2.GetComponentInChildren<TextMesh>().color = Color32.Lerp(Color.clear, new Color32(255,255,255,100), t);
			star3.GetComponent<SpriteRenderer>().color = Color32.Lerp(Color.clear, new Color32(50,50,50,50), t);
			star3.GetComponentInChildren<TextMesh>().color = Color32.Lerp(Color.clear, new Color32(255,255,255,100), t);

			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForSeconds(0.2f);

		if (starsEarned > 0) {
			star1.animation.Play("starin");
			AudioController.Instance.PlaySound("EarnStar");
			yield return new WaitForSeconds(0.3f);
		}
		if (starsEarned > 1) {
			star2.animation.Play("starin");
			AudioController.Instance.PlaySound("EarnStar");
			yield return new WaitForSeconds(0.3f);
		}
		if (starsEarned > 2) {
			star3.animation.Play("starin");
			AudioController.Instance.PlaySound("EarnStar");
			yield return new WaitForSeconds(0.3f);
		}
		this.starsEarned = starsEarned;

		buttons.animation.Play("showButtons");
	}
	
	void ShowLevelSelect () {
		StartCoroutine(IShowLevelSelect());
	}
	
	IEnumerator IShowLevelSelect () {
		yield return new WaitForSeconds(0.2f);
		LevelSelect.Instance.Show();
	}

	void Update () {
		if (InputHandler.GetInstance().inputSignalDown && acceptInputs) {
			Collider2D col = Physics2D.OverlapPoint(GameObject.FindGameObjectWithTag("GUICamera").camera.ScreenToWorldPoint(InputHandler.GetInstance().inputVector), 512);

			if (col) {
				if (col.name == "homeButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					ShowLevelSelect ();
					Hide ();
				}

				else if (col.name == "retryButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					GameController.Instance.Retry();
					Hide ();
				}
			}
		}
	}
	
}
