﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	Transform target;
	bool controlledPan = false;
	const float easeTime = 2;
	AnimationCurve ease = AnimationCurve.EaseInOut(0,0,easeTime,1);
	public static Vector3 slingPos = new Vector3(-3, 2, -10);
	public static Vector3 levelPos = new Vector3(27, 2, -10);

	public void SetTarget (Transform newTarget) {
		target = newTarget;
	}

	public void PanTo (Vector3 panToPosition) {
		if (controlledPan)
			return;
		controlledPan = true;
		target = null;
		StartCoroutine(IPanTo(panToPosition));
	}

	IEnumerator IPanTo (Vector3 panToPosition) {
		float t = 0;
		Vector3 panStartPos = transform.position;
		while (t<easeTime) {
			t = Mathf.Min(t+Time.deltaTime, easeTime);
			transform.position = panStartPos * (1 - ease.Evaluate(t)) + panToPosition * ease.Evaluate(t);
			yield return new WaitForEndOfFrame();
			if (!controlledPan) {
				yield break;
			}
		}
		controlledPan = false;
	}

	public void Reset () {
		target = null;
		controlledPan = false;
		StartCoroutine(QuickPanToReset());
	}

	IEnumerator QuickPanToReset () {
		yield return new WaitForEndOfFrame();
		float t = 0;
		Vector3 panStartPos = transform.position;
		while (t<easeTime) {
			t = Mathf.Min(t+Time.deltaTime * 4, easeTime);
			transform.position = panStartPos * (1 - ease.Evaluate(t)) + slingPos * ease.Evaluate(t);
			yield return new WaitForEndOfFrame();
		}
	}

	void FixedUpdate () {
		if (target && !controlledPan)
			transform.position = new Vector3 (Mathf.Clamp(Mathf.Lerp(transform.position.x, target.position.x, Time.deltaTime * 10), slingPos.x, levelPos.x), 2, -10);
	}
}
