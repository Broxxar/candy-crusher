﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JubjubRandomizer : MonoBehaviour {

	public List<Sprite> jubjubSprites;
	public List<string> jubjubBursts;

	void Awake () {
		int randomColor = Random.Range(0, jubjubSprites.Count); 
		GetComponent<SpriteRenderer>().sprite = jubjubSprites[randomColor];
		GetComponent<Destructible>().deathParticles.Add(jubjubBursts[randomColor]);
	}
}
