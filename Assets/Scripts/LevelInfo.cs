﻿using UnityEngine;
using System.Collections;

public class LevelInfo : MonoBehaviour {
	public int Shots; //how many crushers you get for this level
	public int[] StarScores; // should be an array of size 3, each element is how many points you need to earn that level of star
}
