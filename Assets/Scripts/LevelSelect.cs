using UnityEngine;
using System.Collections;

public class LevelSelect : MonoBehaviour {

	public static LevelSelect Instance {get; private set;}

	LevelIcon[] icons;
	bool acceptInputs;

	void Awake () {
		Instance = this;
		icons = GetComponentsInChildren<LevelIcon>();
		gameObject.SetActive(false);
	}

	public void Show () {
		acceptInputs = true;
		gameObject.SetActive(true);
		animation.Play("levelSelectShow");
		for (int i = 0; i < icons.Length; i++)
			icons[i].GetComponent<LevelIcon>().RefreshIcon();
	}

	public void Hide () {
		acceptInputs = false;
		StartCoroutine(IHide());
		gameObject.SetActive(true);
	}

	IEnumerator IHide () {
		yield return new WaitForSeconds(0.2f);
		animation.Play("levelSelectHide");
	}

	void Select (int levelNumber) {
		StartCoroutine(ISelect(levelNumber));
	}

	IEnumerator ISelect (int levelNumber) {
		yield return new WaitForSeconds(0.2f);
		GameController.Instance.SpawnLevel(levelNumber);
	}

	void Update () {
		if (InputHandler.GetInstance().inputSignalDown && acceptInputs) {
			Collider2D col = Physics2D.OverlapPoint(GameObject.FindGameObjectWithTag("GUICamera").camera.ScreenToWorldPoint(InputHandler.GetInstance().inputVector), 512);

			// TODO: could be improved by having the levelIcons inherit from the InterativeObject
			// and simply tell the LevelSelect instance when they've been tapped

			if (col) {
				if (col.name == "levelIcon") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					if (!col.GetComponent<LevelIcon>().levelLocked) {
						Hide ();
						Select(col.GetComponent<LevelIcon>().LevelNumber);
					}
				}
				else if (col.name == "container") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					Application.OpenURL("https://twitter.com/DanielJMoran");
				}
			}
		}
	}
}
