using UnityEngine;
using System.Collections;

public class InGameButtons : MonoBehaviour {

	bool acceptInputs;

	public static InGameButtons Singleton;

	void Awake () {
		Singleton = this;
	}

	public void Hide () {
		acceptInputs = false;
		animation.Play("ingame_hide");
	}

	public void Show () {
		acceptInputs = true;
		animation.Play("ingame_show");
	}

	void Update () {
		if (InputHandler.GetInstance().inputSignalDown && acceptInputs) {
			Collider2D col = Physics2D.OverlapPoint(GameObject.FindGameObjectWithTag("GUICamera").camera.ScreenToWorldPoint(InputHandler.GetInstance().inputVector), 512);
			
			if (col) {
				if (col.name == "inGameHomeButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					GameController.Instance.Reset(0);
					Hide ();
				}
				
				else if (col.name == "inGameRetryButton") {
					col.animation.Play("buttonPress");
					AudioController.Instance.PlaySound ("ButtonPress");
					GameController.Instance.Reset(1);
					Hide ();
				}
			}
		}
	}
}
