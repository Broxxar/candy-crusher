﻿using UnityEngine;
using UnityEditor;
 
class MapFont : EditorWindow{
	
	[MenuItem("Assets/Map Font")]
	static void Map () {
		
		Vector2 size = new Vector2(456, 455);
		
		string filePath = EditorUtility.OpenFilePanel("Choose Font .txt File","Assets/Fonts/","*");
		filePath = "Assets/Font/" + filePath.Split('/')[filePath.Split('/').Length-1];
		TextAsset textFile = AssetDatabase.LoadAssetAtPath(filePath,typeof(TextAsset)) as TextAsset;
		string[] charData = textFile.text.Split('\n');

		int characterCount = 0;
		int lineHeight = 0;
		const float SPACE_WIDTH = 0.07f;
	
		for (int i = 0; i < charData.Length; i ++) {
			if (charData[i].StartsWith("common lineHeight")) {
				lineHeight = int.Parse(charData[i].Split('=',' ')[2]);
				continue;
			}

			if (charData[i].StartsWith("chars count")) {
				characterCount = int.Parse(charData[i].Split('=',' ')[2]);
				break;
			}
		}

		// characterCount + 1 to include a space character
		CharacterInfo[] chars = new CharacterInfo[characterCount+1];

		for (int i = 0; i < charData.Length; i ++) {
			if (charData[i].ToString().StartsWith("char id")) {
				CharacterInfo charInfo = new CharacterInfo();
				string[] infos = charData[i].Split('=',' ');
				charInfo.index = int.Parse(infos[2]);
			
				charInfo.uv.x = float.Parse(infos[4])/size.x;
				charInfo.uv.y = (size.y - float.Parse(infos[6]))/size.y;
				charInfo.uv.width = float.Parse(infos[8])/size.x;
				charInfo.uv.height = -float.Parse(infos[10])/size.y;

				charInfo.vert.Set(0, (lineHeight - (float.Parse(infos[14]) + float.Parse(infos[10])))/size.y, charInfo.uv.width, -charInfo.uv.height);
				
				charInfo.width = charInfo.uv.width;

				// i-4, because the first 4 lines don't contain characters, so the first character in the file is actually at index 4
				chars[i-4] = charInfo;
			}
		}

		//Add the space character at the end of the array
		CharacterInfo space = new CharacterInfo();
		space.index = 32;
		space.width = SPACE_WIDTH;
		chars[characterCount] = space;

		Font font = new Font ();
		font.characterInfo = chars;
		AssetDatabase.CreateAsset(font, "Assets/Font/font.fontSettings");
		AssetDatabase.Refresh();
	}
}